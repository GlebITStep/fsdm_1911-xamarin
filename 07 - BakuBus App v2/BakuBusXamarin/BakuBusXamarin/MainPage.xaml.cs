﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BakuBusXamarin.Models;
using BakuBusXamarin.Views;
using Xamarin.Forms;

namespace BakuBusXamarin
{
    [DesignTimeVisible(false)]
    public partial class MainPage : MasterDetailPage
    {
        public MainPage()
        {
            InitializeComponent();
            MasterPage.MenuListView.ItemSelected += MenuListViewOnItemSelected;
        }

        private void MenuListViewOnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is MainMenuItem mainMenuItem)
            {
                var page = Activator.CreateInstance(mainMenuItem.PageType) as Page;
                Detail = new NavigationPage(page);
                IsPresented = false;
                MasterPage.MenuListView.SelectedItem = null;
            }
        }
    }
}
