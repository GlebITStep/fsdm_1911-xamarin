﻿using System;
using System.Collections.Generic;
using System.Text;
using BakuBusXamarin.Models;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace BakuBusXamarin.ViewModels
{
    class BusDetailViewModel : ViewModelBase
    {
        private Pin _pin;
        public Pin Pin
        {
            get => _pin;
            set => Set(ref _pin, value);
        }

        public BusDetailViewModel()
        {
            MessagingCenter.Subscribe<HomeViewModel, Pin>(this, "bus_details", (sender, message) =>
            {
                Pin = message;
                Title = message.Label;
            });
        }
    }
}
