﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using BakuBusXamarin.Services;
using BakuBusXamarin.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace BakuBusXamarin.ViewModels
{
    class HomeViewModel : ViewModelBase
    {
        private readonly BakuBusApiClient _bakuBusApiClient;

        public Command SearchCommand { get; set; }
        public Command TestCommand { get; set; }
        public Command BusDetailsCommand { get; set; }


        private ObservableCollection<Pin> _busLocations = new ObservableCollection<Pin>();
        public ObservableCollection<Pin> BusLocations
        {
            get => _busLocations;
            set => Set(ref _busLocations, value);
        }

        private ObservableCollection<string> _busNumbers = new ObservableCollection<string>();
        public ObservableCollection<string> BusNumbers
        {
            get => _busNumbers;
            set => Set(ref _busNumbers, value);
        }

        private string _selectedBusNumber;
        public string SelectedBusNumber
        {
            get => _selectedBusNumber;
            set => Set(ref _selectedBusNumber, value);
        }

        public HomeViewModel()
        {
            _bakuBusApiClient = new BakuBusApiClient();

            SearchCommand = new Command(Search);
            TestCommand = new Command(Test);
            BusDetailsCommand = new Command<Pin>(BusDetails);

            LoadBusNumbers();

            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                    Search();

                return true;
            });
        }

        private async void BusDetails(Pin pin)
        {
            var rootPage = (Application.Current.MainPage as MasterDetailPage).Detail;
            await rootPage.Navigation.PushAsync(new BusDetailPage());
            MessagingCenter.Send(this, "bus_details", pin);
        }

        private async void Test()
        {
            await Application.Current.MainPage.DisplayAlert("Cache", FileSystem.CacheDirectory, "Close");
        }

        private async void LoadBusNumbers()
        {
            var numbers = await _bakuBusApiClient.GetBusNumbersAsync();
            BusNumbers = new ObservableCollection<string>(numbers);
        }

        private async void Search()
        {
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {

                try
                {
                    var busses = (await _bakuBusApiClient.GetBusesAsync(SelectedBusNumber)).ToList();

                    var pins = busses.Select(x => new Pin
                    {
                        Position = new Position(
                            double.Parse(x.Latitude, CultureInfo.GetCultureInfo("ru-RU")),
                            double.Parse(x.Longitude, CultureInfo.GetCultureInfo("ru-RU"))),
                        Label = x.DisplayRouteCode,
                        Address = x.CurrentStop,
                    });

                    BusLocations = new ObservableCollection<Pin>(pins);
                }
                catch (Exception e)
                {
                    //await Application.Current.MainPage.DisplayAlert("Error", "Can't load bus locations!", "Close");
                }

            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Check your internet connection.", "Close");
            }
        }
    }
}
