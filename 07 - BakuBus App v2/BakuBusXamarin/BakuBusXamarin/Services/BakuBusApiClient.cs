﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BakuBusXamarin.Models;
using Newtonsoft.Json;
using Xamarin.Essentials;

namespace BakuBusXamarin.Services
{
    class BakuBusApiClient
    {
        private const string ApiUrl = "https://www.bakubus.az/az/ajax/apiNew1";
        private const string BusNumbersFile = "busnumbers.json";

        private readonly HttpClient _httpClient;


        public BakuBusApiClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<IEnumerable<Bus>> GetBusesAsync(string busNumber = null)
        {
            var json = await _httpClient.GetStringAsync(ApiUrl);
            var response = JsonConvert.DeserializeObject<BakuBusApiResponse>(json);

            if (busNumber is null)
                return response.BusArray
                    .Select(x => x.Bus);
            
            return response.BusArray
                .Select(x => x.Bus)
                .Where(x => x.DisplayRouteCode == busNumber);
        }

        public async Task<IEnumerable<string>> GetBusNumbersAsync()
        {
            var filename = Path.Combine(FileSystem.CacheDirectory, BusNumbersFile);
            var lastUpdatedDate = Preferences.Get("bus_numbers_update_time", DateTime.MinValue);
            var dateDifference = DateTime.Now - lastUpdatedDate;

            if (File.Exists(filename) && dateDifference.Days < 1)
            {
                var json = File.ReadAllText(filename);
                var busNumberList = JsonConvert.DeserializeObject<IEnumerable<string>>(json);
                return busNumberList;
            }
            else
            {
                var json = await _httpClient.GetStringAsync(ApiUrl);
                var response = JsonConvert.DeserializeObject<BakuBusApiResponse>(json);
                var busNumberList = response.BusArray
                    .Select(x => x.Bus.DisplayRouteCode)
                    .Distinct()
                    .OrderBy(x => x);

                var jsonToWrite = JsonConvert.SerializeObject(busNumberList);
                File.WriteAllText(filename, jsonToWrite);

                Preferences.Set("bus_numbers_update_time", DateTime.Now);

                return busNumberList;
            }
        }
    }
}
