﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinTranslator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly TranslationService _translationService;

        public MainPage()
        {
            _translationService = new TranslationService();

            InitializeComponent();
            LoadLanguages();
        }

        public async void LoadLanguages()
        {
            var languages = await _translationService.GetLanguagesAsync("en");

            FromLanguagePicker.ItemsSource = languages.ToList();
            ToLanguagePicker.ItemsSource = languages.ToList();

            if (Application.Current.Properties.ContainsKey("from"))
                FromLanguagePicker.SelectedIndex = (int)Application.Current.Properties["from"];
            else
                FromLanguagePicker.SelectedIndex = 0;

            if (Application.Current.Properties.ContainsKey("to"))
                ToLanguagePicker.SelectedIndex = (int)Application.Current.Properties["to"];
            else
                ToLanguagePicker.SelectedIndex = 1;
        }

        private async void TranslateOnClick(object sender, EventArgs e)
        {
            var langFrom = FromLanguagePicker.SelectedItem as Language;
            var langTo = ToLanguagePicker.SelectedItem as Language;

            try
            {
                var result = await _translationService.TranslateAsync(TextEditor.Text, langFrom.Code, langTo.Code);
                ResultEditor.Text = result;
            }
            catch (Exception exception)
            {
                await DisplayAlert("Error!", "Something went wrong...", "Close");

                //bool result = await DisplayAlert("Error!", "Something went wrong...", "Ok", "Cancel");
                //if (result)
                //    ResultEditor.Text = "Yes";
                //else
                //    ResultEditor.Text = "No";

                //var result = await DisplayActionSheet("Title", "Cancel", null, "One", "Two", "Three");
                //ResultEditor.Text = result;

                //var result = await DisplayPromptAsync("Title", "Enter your name...");
                //ResultEditor.Text = result;
            }
        }

        private void ReverseOnClick(object sender, EventArgs e)
        {
            var index = FromLanguagePicker.SelectedIndex;
            FromLanguagePicker.SelectedIndex = ToLanguagePicker.SelectedIndex;
            ToLanguagePicker.SelectedIndex = index;
        }

        private void FromOnSelectedIndexChanged(object sender, EventArgs e)
        {
            Application.Current.Properties["from"] = FromLanguagePicker.SelectedIndex;
        }

        private void ToOnSelectedIndexChanged(object sender, EventArgs e)
        {
            Application.Current.Properties["to"] = ToLanguagePicker.SelectedIndex;
        }
    }
}
