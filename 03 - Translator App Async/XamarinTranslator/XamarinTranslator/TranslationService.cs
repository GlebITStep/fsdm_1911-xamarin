﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace XamarinTranslator
{
    class TranslationService
    {
        private readonly WebClient _webClient = new WebClient();
        private readonly HttpClient _httpClient = new HttpClient();

        private const string ApiUrl = "https://translate.yandex.net/api/v1.5/tr.json";
        private const string ApiKey = "trnsl.1.1.20190304T131301Z.f6d41bb335d9b8bd.3a5f5291e5292ca3468024f3053f80780fde0e33";
        private const string TranslatePath = "translate";
        private const string GetLanguagesPath = "getLangs";

        public async Task<string> TranslateAsync(string text, string languageCodeFrom, string languageCodeTo)
        {
            var uri = new Uri($"{ApiUrl}/{TranslatePath}")
                .AddQueryParameter("key", ApiKey)
                .AddQueryParameter("text", text)
                .AddQueryParameter("lang", $"{languageCodeFrom}-{languageCodeTo}");

            var json = await _httpClient.GetStringAsync(uri);
            var result = JsonConvert.DeserializeObject<TranslateResponse>(json);

            return result.Text.FirstOrDefault();
        }

        public string Translate(string text, string languageCodeFrom, string languageCodeTo)
        {
            var uri = new Uri($"{ApiUrl}/{TranslatePath}")
                .AddQueryParameter("key", ApiKey)
                .AddQueryParameter("text", text)
                .AddQueryParameter("lang", $"{languageCodeFrom}-{languageCodeTo}");

            var json = _webClient.DownloadString(uri);
            var result = JsonConvert.DeserializeObject<TranslateResponse>(json);

            return result.Text.FirstOrDefault();
        }

        public IEnumerable<Language> GetLanguages(string uiLanguageCode)
        {
            var uri = new Uri($"{ApiUrl}/{GetLanguagesPath}")
                .AddQueryParameter("key", ApiKey)
                .AddQueryParameter("ui", uiLanguageCode);

            var json = _webClient.DownloadString(uri);
            var result = JsonConvert.DeserializeObject<LanguagesListResponse>(json);

            var languages = result.Languages
                .Select(x => new Language { Code = x.Key, Name = x.Value });

            return languages;
        }

        public async Task<IEnumerable<Language>> GetLanguagesAsync(string uiLanguageCode)
        {
            var uri = new Uri($"{ApiUrl}/{GetLanguagesPath}")
                .AddQueryParameter("key", ApiKey)
                .AddQueryParameter("ui", uiLanguageCode);

            var json = await _httpClient.GetStringAsync(uri);
            var result = JsonConvert.DeserializeObject<LanguagesListResponse>(json);

            var languages = result.Languages
                .Select(x => new Language { Code = x.Key, Name = x.Value });

            return languages;
        }
    }
}
