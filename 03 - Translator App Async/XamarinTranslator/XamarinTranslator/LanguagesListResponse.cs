﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace XamarinTranslator
{
    public class LanguagesListResponse
    {
        [JsonProperty("dirs")]
        public string[] Directions { get; set; }

        [JsonProperty("langs")]
        public Dictionary<string, string> Languages { get; set; }
    }
}