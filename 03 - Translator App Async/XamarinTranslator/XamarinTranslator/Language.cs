﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinTranslator
{
    class Language
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
