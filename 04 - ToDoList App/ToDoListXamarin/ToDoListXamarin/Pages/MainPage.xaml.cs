﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ToDoListXamarin.Pages
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void OpenPage_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewPage());
        }

        private async void OpenModal_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NewPage());
        }
    }
}
