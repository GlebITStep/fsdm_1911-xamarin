﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using ToDoListXamarin.Models;
using ToDoListXamarin.Views;
using Xamarin.Forms;

namespace ToDoListXamarin.ViewModels
{
    class MainPageViewModel : ViewModelBase
    {
        private string _title;
        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        private ObservableCollection<ToDoItem> _tasks;
        public ObservableCollection<ToDoItem> Tasks
        {
            get => _tasks;
            set => Set(ref _tasks, value);
        }

        private ToDoItem _selectedItem;
        public ToDoItem SelectedItem
        {
            get => _selectedItem;
            set
            {
                Set(ref _selectedItem, value);
                DetailsCommand.ChangeCanExecute();
            }
        }

        public Command AddTaskCommand { get; set; }
        public Command DetailsCommand { get; set; }

        public MainPageViewModel()
        {
            Title = "TODO List";
            AddTaskCommand = new Command(AddTask);
            DetailsCommand = new Command(Details, CanNavigateToDetails);

            Tasks = new ObservableCollection<ToDoItem>
            {
                new ToDoItem
                {
                    Title = "One",
                    Description = "qqweqwesf sdf sdfsd f",
                    DeadLine = DateTime.Now
                },
                new ToDoItem
                {
                    Title = "Two",
                    Description = "sdfsdffgh fghfg h",
                    DeadLine = DateTime.Now
                },
                new ToDoItem
                {
                    Title = "Three",
                    Description = "jkljkddf gdfg",
                    DeadLine = DateTime.Now
                }
            };
        }

        private bool CanNavigateToDetails()
        {
            return SelectedItem != null;
        }

        private async void Details()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new TaskDetailsView());
            MessagingCenter.Send(this, "details", SelectedItem);
        }

        private async void AddTask()
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(new TaskEditorModal());
        }
    }
}
