﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListXamarin.Models;
using Xamarin.Forms;

namespace ToDoListXamarin.ViewModels
{
    class TaskDetailsViewModel : ViewModelBase
    {
        private string _title;
        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        private string _description;
        public string Description
        {
            get => _description;
            set => Set(ref _description, value);
        }

        private DateTime _deadLine;
        public DateTime DeadLine
        {
            get => _deadLine;
            set => Set(ref _deadLine, value);
        }

        public TaskDetailsViewModel()
        {
            MessagingCenter.Subscribe<MainPageViewModel, ToDoItem>(this, "details", (sender, message) =>
            {
                Title = message.Title;
                Description = message.Description;
                DeadLine = message.DeadLine;
            });
        }
    }
}
