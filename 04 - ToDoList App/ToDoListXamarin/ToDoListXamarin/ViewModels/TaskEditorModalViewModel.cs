﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace ToDoListXamarin.ViewModels
{
    class TaskEditorModalViewModel : ViewModelBase
    {
        public Command CancelCommand { get; set; }
        public Command CreateCommand { get; set; }

        public TaskEditorModalViewModel()
        {
            CancelCommand = new Command(Cancel);
            CreateCommand = new Command(Create);
        }

        private async void Cancel()
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        private async void Create()
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}
