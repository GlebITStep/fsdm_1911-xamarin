﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoListXamarin.Models
{
    class ToDoItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime DeadLine { get; set; }
    }
}
