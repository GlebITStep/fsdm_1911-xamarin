﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToDoListXamarin.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ToDoListXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TaskEditorModal : ContentPage
    {
        public TaskEditorModal()
        {
            InitializeComponent();
            BindingContext = new TaskEditorModalViewModel();
        }
    }
}