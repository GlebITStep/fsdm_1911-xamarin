﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoListXamarin.Tools
{
    class ValidatedObjectBase : ObservableObject
    {
        private bool _isValid;
        public bool IsValid
        {
            get => _isValid;
            protected set => Set(ref _isValid, value);
        }

        private string _error;
        public string Error
        {
            get => _error;
            protected set => Set(ref _error, value);
        }

        public string PropertyName { get; }

        public ValidatedObjectBase(string propertyName)
        {
            PropertyName = propertyName;
            IsValid = true;
            Error = string.Empty;
        }

        public void AddValidationError(string errorText)
        {
            IsValid = false;
            Error = errorText;
        }

        public void ClearValidationErrors()
        {
            IsValid = true;
            Error = string.Empty;
        }
    }

    class ValidatedObject<T> : ValidatedObjectBase
    {
        private T _value;
        public T Value
        {
            get => _value;
            set => Set(ref _value, value);
        }

        public ValidatedObject(string propertyName) : base(propertyName) { }
    }
}
