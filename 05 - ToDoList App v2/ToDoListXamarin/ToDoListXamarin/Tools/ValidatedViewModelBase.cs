﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using FluentValidation;
using FluentValidation.Results;
using ToDoListXamarin.ViewModels;
using Xamarin.Forms.Internals;

namespace ToDoListXamarin.Tools
{
    abstract class ValidatedViewModelBase<T> : ViewModelBase where T : IValidator, new()
    {
        public event Action ValidationCompleted;

        public virtual void OnValidationCompleted() { }

        private ValidationResult Validate()
        {
            var validator = new T();
            return validator.Validate(this);
        }

        public IEnumerable<ValidatedObjectBase> GetValidatedObjects()
        {
            return GetType()
                .GetProperties()
                .Where(x => x.PropertyType.IsSubclassOf(typeof(ValidatedObjectBase)))
                .Select(x => x.GetValue(this) as ValidatedObjectBase);
        }

        protected ValidatedViewModelBase()
        {
            SubscribeToValidatedObjectChanges();
            ValidationCompleted += OnValidationCompleted;
        }

        private void SetValidationResults()
        {
            var result = Validate();

            GetValidatedObjects()
                .ForEach(x => x.ClearValidationErrors());

            if (!result.IsValid)
            {
                var errorGroups = result.Errors
                    .GroupBy(x => x.PropertyName);

                foreach (var errorGroup in errorGroups)
                {
                    var validatedObject = GetValidatedObjects()
                        .FirstOrDefault(x => errorGroup.Key.StartsWith(x.PropertyName));

                    validatedObject?.AddValidationError(errorGroup.First().ErrorMessage);
                }
            }

            ValidationCompleted?.Invoke();
        }

        public void SubscribeToValidatedObjectChanges()
        {
            GetValidatedObjects().ForEach(x => x.PropertyChanged += (sender, args) =>
            {
                if (args.PropertyName == "Value")
                    SetValidationResults();
            });
        }
    }
}
