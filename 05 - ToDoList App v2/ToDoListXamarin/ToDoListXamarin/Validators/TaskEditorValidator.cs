﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using ToDoListXamarin.ViewModels;

namespace ToDoListXamarin.Validators
{
    class TaskEditorValidator : AbstractValidator<TaskEditorModalViewModel>
    {
        public TaskEditorValidator()
        {
            RuleFor(x => x.Title.Value)
                .NotEmpty()
                .MinimumLength(5);

            RuleFor(x => x.Description.Value)
                .NotEmpty()
                .MaximumLength(20);

            RuleFor(x => x.DeadLine.Value)
                .GreaterThan(DateTime.Now);
        }
    }
}
