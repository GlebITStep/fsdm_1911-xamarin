﻿using System;
using System.Collections.Generic;
using System.Text;
using ToDoListXamarin.Models;
using ToDoListXamarin.Tools;
using ToDoListXamarin.Validators;
using Xamarin.Forms;

namespace ToDoListXamarin.ViewModels
{
    class TaskEditorModalViewModel : ValidatedViewModelBase<TaskEditorValidator>
    {
        private ValidatedObject<string> _title = new ValidatedObject<string>(nameof(Title));
        public ValidatedObject<string> Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        private ValidatedObject<string> _description = new ValidatedObject<string>(nameof(Description));
        public ValidatedObject<string> Description
        {
            get => _description;
            set => Set(ref _description, value);
        }

        private ValidatedObject<DateTime> _deadLine = new ValidatedObject<DateTime>(nameof(DeadLine));
        public ValidatedObject<DateTime> DeadLine
        {
            get => _deadLine;
            set => Set(ref _deadLine, value);
        }

        public Command CancelCommand { get; set; }
        public Command CreateCommand { get; set; }

        public TaskEditorModalViewModel()
        {
            CancelCommand = new Command(Cancel);
            CreateCommand = new Command(Create, CanCreate);
        }

        private bool CanCreate()
        {
            return Title.IsValid && Description.IsValid && DeadLine.IsValid;
        }

        private async void Cancel()
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        private async void Create()
        {
            var toDoItem = new ToDoItem
            {
                Title = Title.Value,
                Description = Description.Value,
                DeadLine = DeadLine.Value
            };
            MessagingCenter.Send(this, "newTask", toDoItem);
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }

        public override void OnValidationCompleted()
        {
            CreateCommand.ChangeCanExecute();
        }
    }
}