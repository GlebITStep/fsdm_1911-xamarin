﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using ToDoListXamarin.Annotations;
using ToDoListXamarin.Tools;

namespace ToDoListXamarin.ViewModels
{
    class ViewModelBase : ObservableObject
    {
        private string _title;
        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }
    }
}