﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using ToDoListXamarin.Models;
using ToDoListXamarin.Views;
using Xamarin.Forms;

namespace ToDoListXamarin.ViewModels
{
    class MainPageViewModel : ViewModelBase
    {
        private ObservableCollection<ToDoItem> _tasks;
        public ObservableCollection<ToDoItem> Tasks
        {
            get => _tasks;
            set => Set(ref _tasks, value);
        }

        public Command AddTaskCommand { get; set; }
        public Command DetailsCommand { get; set; }
        public Command SaveCommand { get; set; }
        public Command LoadCommand { get; set; }

        public MainPageViewModel()
        {
            Title = "TODO List";
            AddTaskCommand = new Command(AddTask);
            DetailsCommand = new Command<ToDoItem>(Details);
            SaveCommand = new Command(Save);
            LoadCommand = new Command(Load);

            Tasks = new ObservableCollection<ToDoItem>
            {
                new ToDoItem
                {
                    Title = "One",
                    Description = "qqweqwesf sdf sdfsd f",
                    DeadLine = DateTime.Now
                },
                new ToDoItem
                {
                    Title = "Two",
                    Description = "sdfsdffgh fghfg h",
                    DeadLine = DateTime.Now
                },
                new ToDoItem
                {
                    Title = "Three",
                    Description = "jkljkddf gdfg",
                    DeadLine = DateTime.Now
                }
            };

            MessagingCenter.Subscribe<TaskEditorModalViewModel, ToDoItem>(this, "newTask", (sender, message) =>
            {
                Tasks.Add(message);
            });
        }

        private void Load()
        {
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tasks.json");
            var json = File.ReadAllText(fileName);
            Tasks = JsonConvert.DeserializeObject<ObservableCollection<ToDoItem>>(json);
        }

        private async void Save()
        {
            string fileName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "tasks.json");
            var json = JsonConvert.SerializeObject(Tasks);
            File.WriteAllText(fileName, json);
            await Application.Current.MainPage.DisplayAlert("Saved!", fileName, "Close");
        }

        private async void Details(ToDoItem item)
        {
            await Application.Current.MainPage.Navigation.PushAsync(new TaskDetailsView());
            MessagingCenter.Send(this, "details", item);
        }

        private async void AddTask()
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(new TaskEditorModal());
        }
    }
}
