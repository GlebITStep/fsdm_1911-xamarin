﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using BakuBusXamarin.Services;

namespace BakuBusXamarin.Droid.Services
{
    class AndroidTestService : ITestService
    {
        public string GetText()
        {
            return "Android";
        }
    }
}