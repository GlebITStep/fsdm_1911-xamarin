﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace BakuBusXamarin.Effects
{
    public class RoundEffect : RoutingEffect
    {
        public RoundEffect() : base($"BakuBusXamarin.{nameof(RoundEffect)}")
        {
        }
    }
}
