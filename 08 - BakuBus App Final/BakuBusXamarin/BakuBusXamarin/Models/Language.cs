﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BakuBusXamarin.Models
{
    class Language
    {
        public string Title { get; set; }
        public string Code { get; set; }

        public override string ToString() => Title;
    }
}
