﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BakuBusXamarin.Models;

namespace BakuBusXamarin.Services
{
    internal interface IBakuBusApiClient
    {
        Task<IEnumerable<Bus>> GetBusesAsync(string busNumber = null);
        Task<IEnumerable<string>> GetBusNumbersAsync();
    }
}