﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BakuBusXamarin.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BakuBusXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuPage : ContentPage
    {
        public ListView MenuListView => menuListView;

        public MainMenuPage()
        {
            InitializeComponent();
            BindingContext = new MainMenuViewModel();
        }
    }
}