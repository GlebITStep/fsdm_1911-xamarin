﻿using System;
using System.Globalization;
using BakuBusXamarin.Services;
using BakuBusXamarin.ViewModels;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BakuBusXamarin
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var langCode = Preferences.Get("language", CultureInfo.CurrentUICulture.Name);
            CultureInfo.CurrentUICulture = new CultureInfo(langCode);

            DependencyService.Register<IBakuBusApiClient, BakuBusApiClient>();
            DependencyService.Register<AboutViewModel>();

            DependencyService.Get<INotificationManager>().Initialize();

            MainPage = new MainPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
