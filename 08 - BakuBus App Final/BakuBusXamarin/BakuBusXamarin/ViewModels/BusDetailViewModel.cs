﻿using System;
using System.Collections.Generic;
using System.Text;
using BakuBusXamarin.Models;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace BakuBusXamarin.ViewModels
{
    class BusDetailViewModel : ViewModelBase
    {
        private Bus _bus;
        public Bus Bus
        {
            get => _bus;
            set => Set(ref _bus, value);
        }

        private string _distance;
        public string Distance
        {
            get => _distance;
            set => Set(ref _distance, value);
        }


        public Command ShareBusCommand { get; set; }


        public BusDetailViewModel()
        {
            MessagingCenter.Subscribe<HomeViewModel, Bus>(this, "bus_details", async (sender, message) =>
            {
                Bus = message;
                Title = $"Details about bus number: {message.DisplayRouteCode}";

                var userLocation =  await Geolocation.GetLastKnownLocationAsync();
                var busLocation = new Location(Bus.Latitude, Bus.Longitude);
                var distance = Location.CalculateDistance(userLocation, busLocation, DistanceUnits.Kilometers);
                Distance = $"{distance} km";

                await TextToSpeech.SpeakAsync(Bus.CurrentStop);
            });

            ShareBusCommand = new Command(ShareBus);
        }

        private void ShareBus()
        {
            Share.RequestAsync(new ShareTextRequest
            {
                Text = Bus.CurrentStop,
                Title = "Bus current stop"
            });
        }
    }
}
