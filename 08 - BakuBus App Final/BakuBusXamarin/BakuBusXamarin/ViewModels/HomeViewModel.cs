﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using BakuBusXamarin.Models;
using BakuBusXamarin.Services;
using BakuBusXamarin.Views;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace BakuBusXamarin.ViewModels
{
    class HomeViewModel : ViewModelBase
    {
        private readonly IBakuBusApiClient _bakuBusApiClient;
        private readonly ITestService _testService;
        private readonly INotificationManager _notificationManager;

        public Command SearchCommand { get; set; }
        public Command TestCommand { get; set; }
        public Command<Bus> BusDetailsCommand { get; set; }


        private ObservableCollection<Bus> _busLocations = new ObservableCollection<Bus>();
        public ObservableCollection<Bus> BusLocations
        {
            get => _busLocations;
            set => Set(ref _busLocations, value);
        }

        private ObservableCollection<string> _busNumbers = new ObservableCollection<string>();
        public ObservableCollection<string> BusNumbers
        {
            get => _busNumbers;
            set => Set(ref _busNumbers, value);
        }

        private string _selectedBusNumber;
        public string SelectedBusNumber
        {
            get => _selectedBusNumber;
            set => Set(ref _selectedBusNumber, value);
        }


        public HomeViewModel()
        {
            _bakuBusApiClient = DependencyService.Get<IBakuBusApiClient>();
            _testService = DependencyService.Get<ITestService>();
            _notificationManager = DependencyService.Get<INotificationManager>();

            SearchCommand = new Command(Search);
            TestCommand = new Command(Test);
            BusDetailsCommand = new Command<Bus>(BusDetails);

            LoadBusNumbers();

            Device.StartTimer(TimeSpan.FromSeconds(5), () =>
            {
                if (Connectivity.NetworkAccess == NetworkAccess.Internet)
                    Search();

                return true;
            });
        }

        private async void BusDetails(Bus bus)
        {
            var detailPage = (Application.Current.MainPage as MasterDetailPage).Detail;
            await detailPage.Navigation.PushAsync(new BusDetailPage());

            MessagingCenter.Send(this, "bus_details", bus);
        }

        private async void Test()
        {
            await Application.Current.MainPage.DisplayAlert(
                "Culture",
                System.Globalization.CultureInfo.CurrentUICulture.Name, 
                "Close");

            //var address = await Application.Current.MainPage.DisplayPromptAsync("Address", "Enter address here:");
            //var location = (await Geocoding.GetLocationsAsync(address)).FirstOrDefault();
            //if (location != null)
            //{
            //    await Application.Current.MainPage.DisplayAlert(
            //        "Location",
            //        $"{location.Latitude} - {location.Longitude}",
            //        "Close");
            //}

            //_notificationManager.ScheduleNotification("Title", _testService.GetText());

            //await Application.Current.MainPage.DisplayAlert("Message", _testService.GetText(), "Close");
        }

        private async void LoadBusNumbers()
        {
            var numbers = await _bakuBusApiClient.GetBusNumbersAsync();
            BusNumbers = new ObservableCollection<string>(numbers);
        }

        private async void Search()
        {
            if (Connectivity.NetworkAccess == NetworkAccess.Internet)
            {

                try
                {
                    var busses = await _bakuBusApiClient.GetBusesAsync(SelectedBusNumber);
                    BusLocations = new ObservableCollection<Bus>(busses);


                    //var busses = (await _bakuBusApiClient.GetBusesAsync(SelectedBusNumber)).ToList();

                    //var pins = busses.Select(x => new Pin
                    //{
                    //    Position = new Position(x.Latitude, x.Longitude),
                    //    Label = x.DisplayRouteCode,
                    //    Address = x.CurrentStop,
                    //});

                    //BusLocations = new ObservableCollection<Pin>(pins);
                }
                catch (Exception e)
                {
                    //await Application.Current.MainPage.DisplayAlert("Error", "Can't load bus locations!", "Close");
                }

            }
            else
            {
                await Application.Current.MainPage.DisplayAlert("Error", "Check your internet connection.", "Close");
            }
        }
    }
}
