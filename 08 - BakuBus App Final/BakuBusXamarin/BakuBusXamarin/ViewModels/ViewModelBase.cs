﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace BakuBusXamarin.ViewModels
{
    class ViewModelBase : INotifyPropertyChanged
    {
        private string _title;
        public string Title
        {
            get => _title;
            set => Set(ref _title, value);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        
        protected virtual void Set<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            field = value;
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}