﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using BakuBusXamarin.Models;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace BakuBusXamarin.ViewModels
{
    class AboutViewModel : ViewModelBase
    {
        private ObservableCollection<Language> _languages;
        public ObservableCollection<Language> Languages
        {
            get => _languages;
            set => Set(ref _languages, value);
        }

        private Language _currentLanguage;
        public Language CurrentLanguage
        {
            get => _currentLanguage;
            set => Set(ref _currentLanguage, value);
        }

        public Command SelectLanguageCommand { get; set; }

        public AboutViewModel()
        {
            Languages = new ObservableCollection<Language>
            {
                new Language { Title = "English", Code = "en" },
                new Language { Title = "Russian", Code = "ru" },
            };

            SelectLanguageCommand = new Command(SelectLanguage);
        }

        private async void SelectLanguage()
        {
            CultureInfo.CurrentUICulture = new CultureInfo(CurrentLanguage.Code);

            //await Application.Current.MainPage.DisplayAlert(
            //    "Language",
            //    CultureInfo.CurrentUICulture.Name,
            //    "Close");

            Preferences.Set("language", CurrentLanguage.Code);
        }
    }
}
