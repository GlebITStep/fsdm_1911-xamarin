﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BakuBusXamarin.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using Xamarin.Forms.Xaml;

namespace BakuBusXamarin.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            BindingContext = new HomePageViewModel();

            var position = new Position(40.3897669, 49.8758834);
            MapSpan mapSpan = MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(8));
            Map.MoveToRegion(mapSpan);            
        }
    }
}