﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BakuBusXamarin.Models;
using Newtonsoft.Json;

namespace BakuBusXamarin.Services
{
    class BakuBusApiClient
    {
        private const string ApiUrl = "https://www.bakubus.az/az/ajax/apiNew1";
        private readonly HttpClient _httpClient;

        public BakuBusApiClient()
        {
            _httpClient = new HttpClient();
        }

        public async Task<IEnumerable<Bus>> GetBusesAsync()
        {
            var json = await _httpClient.GetStringAsync(ApiUrl);
            var response = JsonConvert.DeserializeObject<BakuBusApiResponse>(json);
            return response.BusArray.Select(x => x.Bus);
        }
    }
}
