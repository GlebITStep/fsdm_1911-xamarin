﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace BakuBusXamarin.Models
{
    public class BakuBusApiResponse
    {
        [JsonProperty("BUS")]
        public BusObject[] BusArray { get; set; }
    }

    public class BusObject
    {
        [JsonProperty("@attributes")]
        public Bus Bus { get; set; }
    }

    public class Bus
    {
        [JsonProperty("BUS_ID")]
        public string BusId { get; set; }

        [JsonProperty("PLATE")]
        public string Plate { get; set; }

        [JsonProperty("DRIVER_NAME")]
        public string DriverName { get; set; }

        [JsonProperty("CURRENT_STOP")]
        public string CurrentStop { get; set; }

        [JsonProperty("PREV_STOP")]
        public string PrevStop { get; set; }

        [JsonProperty("SPEED")]
        public string Speed { get; set; }

        [JsonProperty("BUS_MODEL")]
        public string BusModel { get; set; }

        [JsonProperty("LATITUDE")]
        public string Latitude { get; set; }

        [JsonProperty("LONGITUDE")]
        public string Longitude { get; set; }

        [JsonProperty("ROUTE_NAME")]
        public string RouteName { get; set; }

        [JsonProperty("LAST_UPDATE_TIME")]
        public string LastUpdateTime { get; set; }

        [JsonProperty("DISPLAY_ROUTE_CODE")]
        public string DisplayRouteCode { get; set; }

        [JsonProperty("SVCOUNT")]
        public string SvCount { get; set; }
    }

}
