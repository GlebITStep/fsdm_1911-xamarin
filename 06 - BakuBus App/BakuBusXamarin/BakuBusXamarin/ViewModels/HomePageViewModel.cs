﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BakuBusXamarin.Services;
using Xamarin.Forms;

namespace BakuBusXamarin.ViewModels
{
    class HomePageViewModel
    {
        private readonly BakuBusApiClient _bakuBusApiClient;

        public Command SearchCommand { get; set; }

        public HomePageViewModel()
        {
            _bakuBusApiClient = new BakuBusApiClient();

            SearchCommand = new Command(Search);
        }

        private async void Search()
        {
            var busses = (await _bakuBusApiClient.GetBusesAsync()).ToList();
            await Application.Current.MainPage.DisplayAlert(busses[0].DisplayRouteCode, busses[0].DriverName, "Close");
        }
    }
}
