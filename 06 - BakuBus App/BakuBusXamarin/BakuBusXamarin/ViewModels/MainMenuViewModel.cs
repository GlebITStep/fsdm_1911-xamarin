﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using BakuBusXamarin.Annotations;
using BakuBusXamarin.Models;
using BakuBusXamarin.Views;
using Xamarin.Forms;

namespace BakuBusXamarin.ViewModels
{
    class MainMenuViewModel : ViewModelBase
    {
        private ObservableCollection<MainMenuItem> _menuItems;

        public ObservableCollection<MainMenuItem> MenuItems
        {
            get => _menuItems;
            set => Set(ref _menuItems, value);
        }

        public MainMenuViewModel()
        {
            MenuItems = new ObservableCollection<MainMenuItem>
            {
                new MainMenuItem { Title = "Home", PageType = typeof(HomePage) },
                new MainMenuItem { Title = "About", PageType = typeof(AboutPage) }
            };
        }
    }
}
