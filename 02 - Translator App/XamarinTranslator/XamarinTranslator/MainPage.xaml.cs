﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinTranslator
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly TranslationService _translationService;

        public MainPage()
        {
            InitializeComponent();

            _translationService = new TranslationService();
            var langauges = _translationService.GetLangauges("en");

            FromLanguagePicker.ItemsSource = langauges.ToList();
            ToLanguagePicker.ItemsSource = langauges.ToList();
        }

        private void TranslateOnClick(object sender, EventArgs e)
        {
            var langFrom = (KeyValuePair<string, string>)FromLanguagePicker.SelectedItem;
            var langTo = (KeyValuePair<string, string>)ToLanguagePicker.SelectedItem;

            var result = _translationService.Translate(TextEditor.Text, langFrom.Key, langTo.Key);
            ResultEditor.Text = result;
        }

        private void ReverseOnClick(object sender, EventArgs e)
        {
            
            
        }
    }
}
