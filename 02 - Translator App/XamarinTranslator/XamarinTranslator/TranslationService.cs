﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json;

namespace XamarinTranslator
{
    class TranslationService
    {
        private const string ApiUrl = "https://translate.yandex.net/api/v1.5/tr.json";
        private const string ApiKey = "trnsl.1.1.20190304T131301Z.f6d41bb335d9b8bd.3a5f5291e5292ca3468024f3053f80780fde0e33";
        private const string TranslatePath = "translate";
        private const string GetLanguagesPath = "getLangs";

        public string Translate(string text, string languageCodeFrom, string languageCodeTo)
        {
            WebClient webClient = new WebClient();

            var uriBuilder = new UriBuilder($"{ApiUrl}/{TranslatePath}");

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["key"] = ApiKey;
            query["text"] = text;
            query["lang"] = $"{languageCodeFrom}-{languageCodeTo}";

            uriBuilder.Query = query.ToString();

            //Debug.WriteLine(uriBuilder.Uri.ToString());

            var json = webClient.DownloadString(uriBuilder.Uri);

            var result = JsonConvert.DeserializeObject<TranslateResponse>(json);

            return result.Text.FirstOrDefault();
        }

        public Dictionary<string, string> GetLangauges(string uiLagnageCode)
        {
            WebClient webClient = new WebClient();

            var uriBuilder = new UriBuilder($"{ApiUrl}/{GetLanguagesPath}");

            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query["key"] = ApiKey;
            query["ui"] = uiLagnageCode;

            uriBuilder.Query = query.ToString();

            var json = webClient.DownloadString(uriBuilder.Uri);

            var result = JsonConvert.DeserializeObject<LanguagesListResponse>(json);

            return result.Languages;
        }
    }
}
